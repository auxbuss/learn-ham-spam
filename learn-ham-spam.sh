#!/bin/bash

echo -e "\nlearning ham\n"

for homeuser in `cat /etc/passwd | grep home | cut -d: -f1`
do
    homedir=`cat /etc/passwd | grep ^$homeuser: | cut -d: -f6`
    if [ -e $homedir/Maildir/".Ham"/cur ]
    then
        sa-learn --ham -u $homeuser --dir $homedir/Maildir/".Ham"/cur
        rm -f $homedir/Maildir/.Ham/cur/*
    fi
done

echo -e "\nlearning spam\n"

for homeuser in `cat /etc/passwd | grep home | cut -d: -f1`
do
    homedir=`cat /etc/passwd | grep ^$homeuser: | cut -d: -f6`
    if [ -e $homedir/Maildir/".Spam"/cur ]
    then
        sa-learn --spam -u $homeuser --dir $homedir/Maildir/".Spam"/cur
        rm -f $homedir/Maildir/.Spam/cur/*
    fi
done

exit 0

This bash script can be used to help spamassassin learn ham and spam.
It runs across all users.

The simplest way to use it is via a cron, say:

    */15 * * * * root /path/to/script/learn-ham-spam.sh 1>/dev/null

Note the script presumes the presence of both:

    $homedir/Maildir/".Ham"/
    $homedir/Maildir/".Spam"/
